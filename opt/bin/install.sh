#!/bin/sh
FILE_PATH=$(readlink -f "$0");
cd $(dirname  "${FILE_PATH}")/../../

echo "Welcome, ${USER}!"
cp .env.example .env
echo "Starting containers.."

docker-compose down && \
docker-compose build --no-cache && \
docker-compose up -d
