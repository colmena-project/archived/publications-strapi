# Colmena project

Colmena is a software solution to create and share content, developed together with local and community media from the Global South. The digital toolbox enables communicators to extend local offline collaborations like interviews, live broadcasting, audio and text edition and into participatory online workflows. Colmena is a free to use, commonly owned and 100% open source. Among it's main features are;



- toolbox for community radios with audio recording and editing tools

- offline mode which allows some tools to be used without Internet

- collaborative space with chat and file sharing

- secure content synchronization to the Colmena cloud

<br />

##  🛠 Requirements
- [Docker - version ˆ20.10.16](https://docs.docker.com/get-docker/)
- [Docker Compose - version ˆ1.29.2](https://docs.docker.com/compose/)

<br />

##  🚀 Getting started

Welcome To Colmena, The one platform/application that empowers journalists around the world with all the necessary tools they need at their fingertips. The project is open-source, which means anyone is welcome to make contributions to improve the codebase.

<br />

### `Installation`
<br />
You start by cloning the project in some local directory by running the following command:

```shell
$ git clone git@git.colmena.network:maia/publications.git && cd publications
```

<br />
Copy .env.example to .env and change envrioment values

```shell
$ cp .env.example .env
```

> See the .env file and change the values what you want

<br />
You can start the CMS running the following command:

```shell
$ docker-compose up
```

<br />

if everything goes well you should see the following log in your terminal
```shell
...
strapi      | 
strapi      |  Actions available
strapi      | 
strapi      | Welcome back!
strapi      | To manage your project 🚀, go to the administration panel at:
strapi      | http://localhost:1337/admin
strapi      | 
strapi      | To access the server ⚡️, go to:
strapi      | http://localhost:1337
...
```

<br />

🚀 You CMS is running on http://localhost:1337/admin

### `How to use Strapi?`

In the [Strapi documentation](https://strapi.io/resource-center) there is information on how we can use it. To work with publications, we will use the strapi **GraphQL API** module and for that we will need to do the following steps:

1. Start the Admin Dashboard
2. [Access the page to create an API access token](http://localhost:1337/admin/settings/api-tokens)
3. Create an access token if you haven’t one
4. Copy the access token to use in your client application if you need
5. Access [the page to release access to API and GraphQL consumption](http://localhost:1337/admin/settings/users-permissions/roles)
6. Click in **public** for edit external API access
![image](https://docs.strapi.io/assets/img/end-user_roles.28f5dbe9.png)
7. Open the collapse and click **select all** for **Publication**, **Category**, **Publication-file** and **Upload**
8. Click in **save button** and [access the GraphQL Playground to test the API](http://localhost:1337/graphql):

Example query, you can test them
```graphql
query FindDraft {
	publications(publicationState: PREVIEW) {
    data {
      attributes {
        title
        description
        content
        identifier
        publishedAt
        tags
        files {
          data {
            attributes {
              title
              description
              media {
                data {
                  attributes {
                    url
                  }
                }
              }
              language
              format
              extent_size
              extent_duration
            }
          }
        }
        date,
        updatedAt
        createdAt
        identifier
        publishedAt
      }
      
    }
  }
}

query FindPublished {
	publications(publicationState: LIVE) {
    data {
      attributes {
        title
        description
        content
        identifier
        publishedAt
        tags
        files {
          data {
            attributes {
              title
              description
              media {
                data {
                  attributes {
                    url
                  }
                }
              }
              language
              format
              extent_size
              extent_duration
            }
          }
        }
        date,
        updatedAt
        createdAt
        identifier
        publishedAt
      }
      
    }
  }
}

mutation CreatePublicationAsDraft {
  createPublication (data: { title: "Hello", description: "World", coverage: "Coverage", publisher: "Teste", rights: "MIT", date: "2022-08-18T13:20:51.567Z", content: "Hi Theare!"}) {
    data {
      id
      attributes {
        title
        description,
        coverage
        publisher
        date
        content
        rights
      }
    }
  }
}

mutation CreatePublicationAsPublicated {
  createPublication (data: { title: "Hello", description: "World", coverage: "Coverage", publisher: "Teste", rights: "MIT", date: "2022-08-18T13:20:51.567Z", content: "I'm alive!", publishedAt: "2022-08-18T13:20:51.567Z" }) {
    data {
      id
      attributes {
        title
        description,
        coverage
        publisher
        date
        content
        rights
      }
    }
  }
}
```


