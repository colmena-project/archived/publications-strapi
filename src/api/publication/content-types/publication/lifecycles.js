module.exports = {
  async beforeDelete(event) {
    const publication = await strapi.query(event.model.uid).findOne({
        where: { id: event.params.where.id, },
        populate: { relation: true, files: true },
    });

    if (!publication) {
        return
    }

    if (publication.files) {
        const promises = publication.files.map(file => strapi.query('api::publication-file.publication-file').delete({
            where: {
                id: file.id
            }
        }))

        await Promise.all(promises)
    }


    if (publication.relation && publication.relation.id) {
        await strapi.query('plugin::upload.file').delete({
            where: {
                id: publication.relation.id
            }
        });
    }
  },
};
