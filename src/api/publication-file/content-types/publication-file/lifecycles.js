module.exports = {
  async beforeDelete(event) {
    const publicationFile = await strapi.query(event.model.uid).findOne({
      where: { id: event.params.where.id },
      populate: { media: true },
    });

    if (publicationFile && publicationFile.media && publicationFile.media.id) {
      await strapi.query("plugin::upload.file").delete({
        where: {
          id: publicationFile.media.id,
        },
      });
    }
  },
};
