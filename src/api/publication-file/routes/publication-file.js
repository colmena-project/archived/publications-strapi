'use strict';

/**
 * publication-file router.
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::publication-file.publication-file');
