'use strict';

/**
 *  publication-file controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::publication-file.publication-file');
