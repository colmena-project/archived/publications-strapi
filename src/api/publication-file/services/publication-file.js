'use strict';

/**
 * publication-file service.
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::publication-file.publication-file');
